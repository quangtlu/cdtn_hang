<?php

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;
use Spatie\Permission\PermissionRegistrar;
use App\Models\User;
use Illuminate\Support\Facades\Hash;

class PermissionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */

    public function run()
    {
        app()[PermissionRegistrar::class]->forgetCachedPermissions();

        // permission
        foreach (config('permission.module-all') as $module) {
            foreach (config('permission.action') as $action) {
                Permission::create(['name' => $action.'-'.$module]);
            }
        }
 
        $roleAdmin = Role::create(['name' => 'admin']);
        Role::create(['name' => 'user']);
        $roleAdmin->givePermissionTo(Permission::all());
        $admin =  User::create([
            'name' => 'admin',
            'email' => 'admin@gmail.com',
            'phone' => '0900000001',
            'gender' => config('consts.user.gender.male.value'),
            'password' => Hash::make('password')
        ]);

        // assignRole to user
        $admin->assignRole($roleAdmin);
    }
}
