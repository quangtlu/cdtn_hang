<?php

use App\Models\Category;
use App\Models\PostCategory;
use Illuminate\Database\Seeder;

class CategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
            Category::insert(
                ['name' => 'Giới thiệu'],
                ['name' => 'Văn bản pháp luật'],
                ['name' => 'Hỏi và đáp'],
            );

            $cate1 = Category::create(['name' => 'Về quyền SH công nghiệp (SHCN)']);
            $cate2 = Category::create(['name' => 'Về quyền tác giả']);
            $cate3 = Category::create(['name' => 'Về quyền liên quan']);
            $cate4 = Category::create(['name' => 'Sử dụng các tác phẩm được bảo hộ về QTG trong giảng dạy']);
            $cate5 = Category::create(['name' => 'Tác phẩm của Giảng viên và việc xuất bản']);
            $cate6 = Category::create(['name' => 'Hệ thống thực hành quyền tác giả']);

            Category::insert([
                [
                    'name' => 'Quyền SHCN là gì',
                ],
                [
                    'name' => 'Đăng ký SHCN',
                ],
                [
                    'name' => 'Sử dụng SHCN',
                ],
            ]);
            Category::insert([
                [
                    'name' => 'Quyền tác giả là gì',
                ],
                [
                    'name' => 'Tác giả và Chủ sở hữu quyền',
                ],
                [
                    'name' => 'Về đăng ký QTG',
                ],
                [
                    'name' => 'Các quyền độc quyền',
                ],
                [
                    'name' => 'Khu vực công cộng',
                ],
                [
                    'name' => 'Ngoại lệ và Giới hạn quyền',
                ],
                [
                    'name' => 'Xin phép và Cấp phép',
                ],
                [
                    'name' => 'Giấy phép CC',
                ],
                [
                    'name' => 'Quản lý tập thể quyền tác giả',
                ],
            ]);
            Category::insert([
                [
                    'name' => 'Quyền liên quan là gì',
                ],
                [
                    'name' => 'Chủ sở hữu quyền liên quan',
                ],
                [
                    'name' => 'Ngoại lệ và giới hạn quyền',
                ],
                [
                    'name' => 'Xin phép và Cấp phép',
                ],
                [
                    'name' => 'Quản lý tập thể quyền liên quan',
                ],
            ]);
            Category::insert([
                [
                    'name' => 'Sử dụng tác phẩm trong kho tài sản của Trường',
                ],
                [
                    'name' => 'Sử dụng tác phẩm của bên thứ ba trong các bài giảng',
                ],
                [
                    'name' => 'Sử dụng hình ảnh, video, audio',
                ],
                [
                    'name' => 'Sử dụng “không trực tiếp” (transformative)',
                ],
                [
                    'name' => 'Các khóa MOOC và các website tác giả',
                ],
                [
                    'name' => 'Sử dụng Giấy phép CC',
                ],
            ]);
            Category::insert([
                [
                    'name' => 'Hợp đồng xuất bản',
                ],
                [
                    'name' => 'Bài báo công bố trên tạp chí',
                ],
                [
                    'name' => 'Giáo trình',
                ],
                [
                    'name' => 'Bài giảng, các học liệu dùng trong giảng day.',
                ],
                [
                    'name' => 'Bài giảng online',
                ],
                [
                    'name' => 'Sao chép cho sinh viên',
                ],
            ]);
            Category::insert([
                [
                    'name' => '“Ba chân kiềng” của một hệ thống bảo hộ quyền tác giả',
                ],
                [
                    'name' => 'Các CMO tại Việt Nam',
                ],
            ]);
    }
}
