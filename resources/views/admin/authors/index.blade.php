@extends('layouts.admin')
@section('title', 'Quản lý tác giả')
@section('content')
    <div class="content-wrapper">
        <!-- Main content -->
        <div class="content">
            <div class="container-fluid">
                <div class="row">
                    <div class="py-3">
                        <a class="btn btn-success btn-sm"
                            href="{{ route('admin.authors.create') }}">Thêm mới</a>
                    </div>
                    <div class="col-md-12 card">
                        <table class="table">
                            <thead>
                                <tr>
                                    <th>ID</th>
                                    <th>Họ tên</th>
                                    <th>Giới tính</th>
                                    <th>Ngày sinh</th>
                                    <th>Email</th>
                                    <th>Số điện thoại</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($authors as $author)
                                    <tr>
                                        <td>{{ $author->id }}</td>
                                        <td>{{ $author->name }}</td>
                                        <td>
                                            @foreach (config('consts.user.gender') as $gender)
                                                @if ($gender['value'] == $author->gender)
                                                    {{ $gender['name'] }}
                                                @endif 
                                            @endforeach
                                        </td>
                                        <td>{{ $author->dob }}</td>
                                        <td>{{ $author->email }}</td>
                                        <td>{{ $author->phone }}</td>
                                        <td>
                                            <a href="{{ route('admin.authors.edit', ['id' => $author->id]) }}"><button
                                                    class="btn btn-info btn-sm">Sửa</button></a>
                                            <button type="button"
                                                data-url="{{ route('admin.authors.destroy', ['id' => $author->id]) }}"
                                                class="btn btn-danger btn-sm btn-delete">Xóa</button>
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                        {{ $authors->withQueryString()->links() }}
                    </div>
                </div>
                <!-- /.row -->
            </div><!-- /.container-fluid -->
        </div>
        <!-- /.content -->
    </div>
@endsection

