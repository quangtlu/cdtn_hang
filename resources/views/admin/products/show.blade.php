@extends('layouts.admin')
@section('title', 'Chi tiết tác phẩm')
@section('css')
    <link
    rel="stylesheet"
    href="https://unpkg.com/swiper@8/swiper-bundle.min.css"
    />
    <link rel="stylesheet" href="{{ asset('admin/product/show.css') }}">
@endsection
@section('content')
    <div class="content-wrapper">
    <!-- product -->
        <div class="product-content product-wrap clearfix product-deatil">
            <div class="row">
                <div class="col-md-12">
                    <h2 class="name">
                        {{ $product->name }}
                    </h2>
                    <hr />
                    <div class="">
                        <ul class="list-group">
                            <li class="list-group-item">
                                Tác giả:  
                                @if ($product->authors->count())
                                    @foreach ($product->authors as $index => $author)
                                        {{ $index != $product->authors->count() - 1 ? $author->name. ' ,' : $author->name}}
                                    @endforeach
                                @else
                                    Chưa xác định
                                @endif
                            </li>
                            <li class="list-group-item">
                                Chủ sở hữu: {{ ($product->owner->name) ?? '' }}
                            </li>
                            <li class="list-group-item">
                                Ngày sáng tác: {{ $product->pub_date }}
                            </li>
                            <li class="list-group-item">
                                Ngày đăng kí bản quyền: {{ $product->regis_date }}
                            </li>
                        </ul>
                    </div>

                </div>
            </div>
        </div>
    <!-- end product -->
    </div>
@endsection
