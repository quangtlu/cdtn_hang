@extends('layouts.admin')
@section('title', 'Quản lý hợp đồng')
@section('css')
    <link rel="stylesheet" href="{{ asset('admin/contract/index.css') }}" />
@endsection
@section('content')
    <div class="content-wrapper">
        <!-- Main content -->
        <div class="content">
            <div class="container-fluid">
                <div class="row">
                    <div class="py-3">
                        <a class="btn btn-success btn-sm"
                            href="{{ route('admin.contracts.create') }}">Thêm mới</a>
                    </div>
                    <div class="col-md-12 card">
                        <table class="table">
                            <thead>
                                <tr>
                                    <th>ID</th>
                                    <th>Tên hợp đồng</th>
                                    <th>Tác phẩm</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($contracts as $contract)
                                    <tr>
                                        <td>{{ $contract->id }}</td>
                                        <td>{{ Str::ucfirst($contract->name) }}</td>
                                        <td>{{ $contract->product->name }}</td>
                                        <td>
                                            <a href="{{ route('admin.contracts.edit', ['id' => $contract->id]) }}"><button
                                                    class="btn btn-info btn-sm"><i class="fas fa-edit"></i></button></a>
                                        </td>
                                        <td><button type="button"
                                            data-url="{{ route('admin.contracts.destroy', ['id' => $contract->id]) }}"
                                            class="btn btn-danger btn-sm btn-delete"><i class="fas fa-trash-alt"></i></i></button></td>
                                        <td><button data-toggle="modal" data-target="#exampleModalLong-{{ $contract->id }}"
                                            class="btn btn-info btn-sm"><i class="fas fa-eye"></i></button></td>

                                    </tr>
                                    <div class="modal fade" id="exampleModalLong-{{ $contract->id }}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
                                        <div class="modal-dialog" role="document">
                                          <div class="modal-content">
                                            <div class="modal-header">
                                              <h5 class="modal-title" id="exampleModalLongTitle">Chi tiết hợp đồng</h5>
                                              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                <span aria-hidden="true">&times;</span>
                                              </button>
                                            </div>
                                            <div class="modal-body">
                                                <ul class="list-group list-group-flush">
                                                    <li class="list-group-item"><b>Tên hợp đồng: </b>{{ $contract->name }}</li>
                                                    <li class="list-group-item"><b>Tác phẩm: </b>{{ $contract->product->name }}</li>
                                                    <li class="list-group-item"><b>Người dùng: </b>{{ $contract->user->name }}</li>
                                                    <li class="list-group-item"><b>Chủ sở hữu: </b>{{ $contract->product->owner->name }}</li>
                                                    <li class="list-group-item"><b>Giấy phép: </b>{{ $contract->license->name }}</li>
                                                    <li class="list-group-item"><b>Lệ phí: </b>{{number_format($contract->license->fee, 0, '', ',').' VNĐ'  }}</li>
                                                    <li class="list-group-item"><b>Thời hạn: </b>{{ 'Từ ngày '.$contract->start_date.' đến ngày '.$contract->end_date }}</li>
                                                    <li class="list-group-item"><b>Mô tả: </b>{!! $contract->description !!}</li>
                                                </ul>
                                            </div>
                                            <div class="modal-footer">
                                              <button type="button" class="btn btn-secondary" data-dismiss="modal">Đóng</button>
                                            </div>
                                          </div>
                                        </div>
                                      </div>
                                @endforeach
                            </tbody>
                        </table>
                        {{ $contracts->withQueryString()->links() }}
                    </div>
                </div>
                <!-- /.row -->
            </div><!-- /.container-fluid -->
        </div>
        <!-- /.content -->
    </div>
@endsection

