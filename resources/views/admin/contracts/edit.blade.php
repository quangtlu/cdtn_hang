@extends('layouts.admin')
@section('title', 'chỉnh sửa hợp đồng')
@section('css')
    <link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet" />
    <link rel="stylesheet" href="{{ asset('admin/user/create.css') }}">
@endsection
@section('content')
    <div class="content-wrapper">
        <div class="content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-12">
                        <form action="{{ route('admin.contracts.update', ['id' => $contract->id]) }}" method="POST">
                            @csrf
                            <div class="form-group pt-2">
                                <label class="label-required" for="category_name">Tên hợp đồng</label>
                                <input type="text" name="name" class="form-control" value="{{ old('name') ?? $contract->name }}">
                                @error('name')
                                    <span class="mt-1 text-danger">{{ $message }}</span>
                                @enderror
                            </div>
                            <div class="form-group">
                                <label class="label-required" for="category_name">Mô tả</label>
                                <textarea name="description" id="editor" cols="30" rows="10">{{ old('description') ?? $contract->description }}</textarea>
                                @error('description')
                                    <span class="mt-1 text-danger">{{ $message }}</span>
                                @enderror
                            </div>
                            <div class="form-group">
                                <label class="label-required" for="category_name">Ngày bắt đầu hợp đồng</label>
                                <input type="date" class="form-control date-time" name="start_date" value="{{ old('start_date') ?? $contract->start_date }}">
                                @error('start_date')
                                    <span class="mt-1 text-danger">{{ $message }}</span>
                                @enderror
                            </div>
                            <div class="form-group">
                                <label class="label-required" for="category_name">Ngày kết thúc hợp đồng</label>
                                <input type="date" class="form-control date-time" name="end_date" value="{{ old('end_date') ?? $contract->end_date }}">
                                @error('end_date')
                                    <span class="mt-1 text-danger">{{ $message }}</span>
                                @enderror
                            </div>
                            <div class="form-group">
                                <label class="label-required" for="category_name">Người dùng</label>
                                <select name="user_id" class="form-control">
                                    <option value=""></option>
                                    @foreach ($users as $user)
                                        <option value="{{ $user->id }}" @if(old('user_id') == $user->id || $contract->user_id == $user->id) selected @endif>{{ $user->name }}</option>
                                    @endforeach 
                                </select>
                                @error('user_id')
                                    <span class="mt-1 text-danger">{{ $message }}</span>
                                @enderror
                            </div>
                            <div class="form-group">
                                <label class="label-required" for="category_name">Tác phẩm</label>
                                <select name="product_id" class="form-control">
                                    <option value=""></option>
                                    @foreach ($products as $product)
                                        <option value="{{ $product->id }}" @if(old('product_id') == $product->id || $contract->product_id == $product->id) selected @endif>{{ $product->name }}</option>
                                    @endforeach
                                </select>
                                @error('product_id')
                                    <span class="mt-1 text-danger">{{ $message }}</span>
                                @enderror
                            </div>
                            <div class="form-group">
                                <label class="label-required" for="category_name">Giấy phép</label>
                                <select name="license_id" class="form-control">
                                    <option value=""></option>
                                    @foreach ($licenses as $license)
                                        <option value="{{ $license->id }}" @if(old('license_id') == $license->id || $contract->license_id == $license->id) selected @endif>{{ $license->name }}</option>
                                    @endforeach
                                </select>
                                @error('license_id')
                                    <span class="mt-1 text-danger">{{ $message }}</span>
                                @enderror
                            </div>
                            <button type="submit" class="btn btn-primary mb-3">Cập nhật</button>
                        </form>
                    </div>
                </div>
                <!-- /.row -->
            </div><!-- /.container-fluid -->
        </div>
        <!-- /.content -->
    </div>
@endsection
@section('js')
    <script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>
    <script src="https://cdn.ckeditor.com/ckeditor5/34.2.0/classic/ckeditor.js"></script>
    <script src="{{ asset('admin/contract/add.js') }}"></script>
    <script>
        ClassicEditor
            .create(document.querySelector('#editor'))
            .catch(error => {
                console.error(error);
            });

    </script>
@endsection
