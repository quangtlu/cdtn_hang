@extends('layouts.admin')
@section('title', 'Quản lý giấy phép')
@section('content')
    <div class="content-wrapper">
        <!-- Main content -->
        <div class="content">
            <div class="container-fluid">
                <div class="row">
                    <div class="py-3">
                        <a class="btn btn-success btn-sm"
                            href="{{ route('admin.licenses.create') }}">Thêm mới</a>
                    </div>
                    <div class="col-md-12 card">
                        <table class="table">
                            <thead>
                                <tr>
                                    <th>ID</th>
                                    <th>Tên</th>
                                    <th>Mô tả</th>
                                    <th>Chi phí</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($licenses as $license)
                                    <tr>
                                        <td>{{ $license->id }}</td>
                                        <td>{{ $license->name }}</td>
                                        <td>{{ $license->description }}</td>
                                        <td>{{ $license->fee }}</td>
                                        <td>
                                            <a href="{{ route('admin.licenses.edit', ['id' => $license->id]) }}"><button
                                                    class="btn btn-info btn-sm">Sửa</button></a>
                                            <button type="button"
                                                data-url="{{ route('admin.licenses.destroy', ['id' => $license->id]) }}"
                                                class="btn btn-danger btn-sm btn-delete">Xóa</button>
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                        {{ $licenses->withQueryString()->links() }}
                    </div>
                </div>
                <!-- /.row -->
            </div><!-- /.container-fluid -->
        </div>
        <!-- /.content -->
    </div>
@endsection

