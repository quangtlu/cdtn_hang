@extends('layouts.admin')
@section('title', 'Quản lý chủ sở hữu')
@section('content')
    <div class="content-wrapper">
        <!-- Main content -->
        <div class="content">
            <div class="container-fluid">
                <div class="row">
                    <div class="py-3">
                        <a class="btn btn-success btn-sm"
                            href="{{ route('admin.owners.create') }}">Thêm mới</a>
                    </div>
                    <div class="col-md-12 card">
                        <table class="table">
                            <thead>
                                <tr>
                                    <th>ID</th>
                                    <th>Tên chủ sơ hữu</th>
                                    <th>Email</th>
                                    <th>Phone</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($owners as $owner)
                                    <tr>
                                        <td>{{ $owner->id }}</td>
                                        <td>{{ $owner->name }}</td>
                                        <td>{{ $owner->email }}</td>
                                        <td>{{ $owner->phone }}</td>
                                        <td>
                                            <a href="{{ route('admin.owners.edit', ['id' => $owner->id]) }}"><button
                                                    class="btn btn-info btn-sm">Sửa</button></a>
                                            <button type="button"
                                                data-url="{{ route('admin.owners.destroy', ['id' => $owner->id]) }}"
                                                class="btn btn-danger btn-sm btn-delete">Xóa</button>
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                        {{ $owners->withQueryString()->links() }}
                    </div>
                    <!-- /.row -->
                </div><!-- /.container-fluid -->
            </div>
            <!-- /.content -->
        </div>
    </div>
    @endsection
    @section('js')
        <script>
            $('#header-search-form').attr('action', '{{ route('admin.owners.index') }}');
            $('#search-input').attr('placeholder', 'Tìm kiếm chủ sở hữu, email, số điện thoại...');

            $(document).ready(function() {
                $('#search').click(function() {
                    $('#toggle').slideToggle();
                });
            });
        </script>
    @endsection
