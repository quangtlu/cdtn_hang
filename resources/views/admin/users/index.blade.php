@extends('layouts.admin')
@section('title', 'Quản lý người dùng')
@section('css')
    <link rel="stylesheet" href="{{ asset('admin/product/index.css') }}" />
    <style>
        .filter-option-dafault {
            font-weight: bold;
        }

        .filter-user {
            max-width: 15%;
            margin: 0 5px;
        }
    </style>
@endsection
@section('content')
    <div class="content-wrapper">
        <div class="content">
            <div class="container-fluid">
                <div class="row">
                    <div class="py-3">
                        <a class="btn btn-success btn-sm"
                            href="{{ route('admin.users.create') }}">Thêm mới</a>
                    </div>
                    <div class="col-md-12 card">
                        <table class="table">
                            <thead>
                                <tr>
                                    <th>ID</th>
                                    <th>Họ tên</th>
                                    <th>Giới tính</th>
                                    <th>Ngày sinh</th>
                                    <th>Email</th>
                                    <th>Số điện thoại</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($users as $user)
                                    <tr>
                                        <td>{{ $user->id }}</td>
                                        <td>{{ $user->name }}</td>
                                        <td>
                                            @foreach (config('consts.user.gender') as $gender)
                                                @if ($gender['value'] == $user->gender)
                                                    {{ $gender['name'] }}
                                                @endif 
                                            @endforeach
                                        </td>
                                        <td>{{ $user->dob }}</td>
                                        <td>{{ $user->email }}</td>
                                        <td>{{ $user->phone }}</td>
                                        <td>
                                            <a href="{{ route('admin.users.edit', ['id' => $user->id]) }}"><button
                                                    class="btn btn-info btn-sm">Sửa</button></a>
                                            <button type="button"
                                                data-url="{{ route('admin.users.destroy', ['id' => $user->id]) }}"
                                                class="btn btn-danger btn-sm btn-delete">Xóa</button>
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                        {{ $users->withQueryString()->links() }}
                    </div>
                </div>
                <!-- /.row -->
            </div><!-- /.container-fluid -->
        </div>
        <!-- /.content -->
    </div>
@endsection

