@section('css')
    <link rel="stylesheet" href="{{ asset('admin/profile/index.css') }}">
@endsection
<aside class="main-sidebar sidebar-dark-primary elevation-4">
    <!-- Brand Logo -->
    <a href="/" class="brand-link">
        <span class="brand-text font-weight-light"><h5 style="text-align: center">Trang chủ</h5></span>
    </a>

    <!-- Sidebar -->
    <div class="sidebar">

        <!-- Sidebar Menu -->
        <nav class="mt-2">
            <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu"
                data-accordion="false">
                @can('list-user')
                    <li class="nav-item">
                        <a href="{{ route('admin.users.index') }}"
                            class="nav-link {{ Request::is('admin/users*') ? 'active' : '' }}">
                            <i class="fas fa-users"></i>
                            <p>
                                Quản lý người dùng
                            </p>
                        </a>
                    </li>
                @endcan
                @can('list-role')
                    <li class="nav-item">
                        <a href="{{ route('admin.roles.index') }}"
                            class="nav-link {{ Request::is('admin/roles*') ? 'active' : '' }}">
                            <i class="fas fa-user-tag"></i>
                            <p>
                                Quản lý vai trò
                            </p>
                        </a>
                    </li>
                @endcan
                @can('list-permission')
                    <li class="nav-item">
                        <a href="{{ route('admin.permissions.index') }}"
                            class="nav-link {{ Request::is('admin/permissions*') ? 'active' : '' }}">
                            <i class="fas fa-user-shield"></i>
                            <p>
                                Quản lý quyền truy cập
                            </p>
                        </a>
                    </li>
                @endcan
                @can('list-author')
                    <li class="nav-item">
                        <a href="{{ route('admin.authors.index') }}"
                            class="nav-link {{ Request::is('admin/authors*') ? 'active' : '' }} ">
                            <i class="fas fa-user-edit"></i>
                            <p>
                                Quản lý tác giả
                            </p>
                        </a>
                    </li>
                @endcan
                @can('list-product')
                    <li class="nav-item">
                        <a href="{{ route('admin.products.index') }}"
                            class="nav-link {{ Request::is('admin/products*') ? 'active' : '' }}">
                            <i class="fas fa-book"></i>
                            <p>
                                Quản lý tác phẩm
                            </p>
                        </a>
                    </li>
                @endcan
                @can('list-owner')
                    <li class="nav-item">
                        <a href="{{ route('admin.owners.index') }}"
                            class="nav-link {{ Request::is('admin/owners*') ? 'active' : '' }}">
                            <i class="fas fa-user-tie"></i>
                            <p>
                                Quản lý chủ sở hữu
                            </p>
                        </a>
                    </li>
                @endcan
                @can('list-license')
                    <li class="nav-item">
                        <a href="{{ route('admin.licenses.index') }}"
                            class="nav-link {{ Request::is('admin/licenses*') ? 'active' : '' }}">
                            <i class="fas fa-id-badge"></i>
                            <p>
                                Quản lý giấy phép
                            </p>
                        </a>
                    </li>
                @endcan
                @can('list-contract')
                    <li class="nav-item">
                        <a href="{{ route('admin.contracts.index') }}"
                            class="nav-link {{ Request::is('admin/contracts*') ? 'active' : '' }}">
                            <i class="fas fa-id-badge"></i>
                            <p>
                                Quản lý hợp đồng
                            </p>
                        </a>
                    </li>
                @endcan
                @can('list-category')
                    <li class="nav-item">
                        <a href="{{ route('admin.categories.index') }}"
                            class="nav-link {{ Request::is('admin/categories*') ? 'active' : '' }}">
                            <i class="fas fa-list-alt"></i>
                            <p>
                                Quản lý mục lục
                            </p>
                        </a>
                    </li>
                @endcan
                <li class="nav-item">
                    <a class="nav-link text-danger" href="{{ route('logout') }}"
                        onclick="event.preventDefault();
                                        document.getElementById('logout-form').submit();">
                        <i class="fas fa-sign-out-alt"></i>
                        <p>Đăng xuất</p>
                    </a>
                    <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                        @csrf
                        <input type="hidden" name="url_redirect_name" value="admin.dashboard">
                    </form>
                </li>
            </ul>
        </nav>
        <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
</aside>
