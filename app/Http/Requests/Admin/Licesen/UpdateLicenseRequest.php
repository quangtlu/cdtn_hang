<?php

namespace App\Http\Requests\Admin\Licesen;
use Illuminate\Validation\Rule;
use Illuminate\Foundation\Http\FormRequest;

class UpdateLicenseRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => ['required',Rule::unique('licenses','name')->ignore($this->id)],
            'fee' => 'required',
            'description' => 'required',
        ];
    }

    public function messages()
    {
        return [
            'name.required' => 'Vui lòng nhập tên giấy phép',
            'name.unique' => 'Tên giấy phép đã tồn tại',
            'fee.required' => 'Vui lòng nhập chi phí',
            'description.required' => 'Vui lòng nhập mô tả',
        ];
    }
}
