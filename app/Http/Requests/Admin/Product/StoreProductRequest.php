<?php

namespace App\Http\Requests\Admin\Product;

use Illuminate\Foundation\Http\FormRequest;

class StoreProductRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|unique:products',
            'pub_date' => 'required|before:today',
            'regis_date' => 'required|before:today',
            'author_id' => 'required',
            'owner_id' => 'required',
        ];
    }

    public function messages()
    {
        return [
            'regis_date.required' => 'Vui lòng nhập ngày đăng kí tác phẩm',
            'pub_date.required' => 'Vui lòng nhập ngày xuất bản tác phẩm',
            'name.required' => 'Vui lòng nhập tên tác phẩm',
            'name.unique' => 'Tên tác phẩm đã tồn tại',
            'pub_date.before' => 'Ngày xuất bản tác phẩm không được là ngày trong tương lai',
            'regis_date.before' => 'Ngày đăng kí tác phẩm không được là ngày trong tương lai',
            'author_id.required' => 'Vui lòng chọn tác giả',
            'owner_id.required' => 'Vui lòng chọn chủ sở hữu',
        ];
    }
}
