<?php

namespace App\Http\Requests\Admin\Contract;

use Illuminate\Foundation\Http\FormRequest;

class StoreContractRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|unique:contracts',
            'description' => 'required',
            'start_date' => 'required',
            'end_date' => 'required|after:start_date',
            'user_id' => 'required',
            'product_id' => 'required',
            'license_id' => 'required',
        ];
    }

    public function messages()
    {
        return [
            'name.required' => 'Vui lòng nhập tên hợp đồng',
            'name.unique' => 'Tên hợp đồng đã tồn tại',
            'description.required' => 'Vui lòng nhập mô tả hợp đồng',
            'start_date.required' => 'Vui lòng nhập ngày bắt đầu hợp đồng',
            'end_date.required' => 'Vui lòng nhập ngày kết thúc hợp đồng',
            'end_date.after' => 'Ngày kết thúc hợp đồng phải sau ngày bắt đầu',
            'user_id.required' => 'Vui lòng chọn người dùng',
            'product_id.required' => 'Vui lòng chọn tác phẩm',
            'license_id.required' => 'Vui lòng chọn giấy phép',
        ];
    }
}
