<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\Contract\StoreContractRequest;
use App\Http\Requests\Admin\Contract\UpdateContractRequest;
use App\Models\License;
use App\Models\Product;
use App\Models\User;
use App\Services\ContractService;

class ContractController extends Controller
{
    private $contractService;

    public function __construct(ContractService $contractService) {
        $this->contractService = $contractService;
        $users = User::all();
        $licenses = License::all();
        $products = Product::all();

        view()->share(['users' => $users, 'licenses' => $licenses, 'products' => $products]);
    }

    public function index()
    {
        $contracts = $this->contractService->getPaginate();
        return view('admin.contracts.index', compact('contracts'));
    }

    public function create()
    {
        return view('admin.contracts.create');
    }

    public function store(StoreContractRequest $request)
    {
        $this->contractService->create($request);
        return Redirect(route('admin.contracts.index'))->with('success', 'Thêm hợp đồng thành công');
    }

    public function show($id)
    {
        $contract = $this->contractService->getById($id);
        return view('admin.contracts.show', compact('contract'));
    }

    public function edit($id)
    {
        $contract = $this->contractService->getById($id);
        return view('admin.contracts.edit', compact('contract'));
    }

    public function update(UpdateContractRequest $request, $id)
    {
        $this->contractService->update($request, $id);
        return Redirect(route('admin.contracts.index'))->with('success', 'Cập nhật hợp đồng thành công');
    }

    public function destroy($id)
    {
        $contract = $this->contractService->delete($id);
        return response()->json(['contract' => $contract, 'message' => 'Xóa hợp đồng thành công']);
    }
}
