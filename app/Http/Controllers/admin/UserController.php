<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\Admin\User\StoreUserRequest;
use App\Http\Requests\Admin\User\UpdateUserRequest;
use App\Services\RoleService;
use App\Services\UserService;
use function redirect;
use function view;
use App\Http\Controllers\Controller;
use App\Services\CategoryService;
use Illuminate\Http\Request;

class UserController extends Controller
{
    private $userService;
    private $roleSercice;
    private $categoryService;

    public function __construct(UserService $userService, RoleService $roleSercice, CategoryService $categoryService)
    {
        $this->userService = $userService;
        $this->roleSercice = $roleSercice;
        $this->categoryService = $categoryService;
        $roles = $this->roleSercice->getAll();
        view()->share(['roles' => $roles]);
    }

    public function index(Request $request)
    {
        $users = $this->userService->getPaginate();
        return view('admin.users.index', compact('users'));
    }

    public function create()
    {
        return view('admin.users.create');
    }

    public function store(StoreUserRequest $request)
    {
        $this->userService->create($request);
        return Redirect(route('admin.users.index'))->with('success', 'Thêm thành công');
    }

    public function edit($id)
    {
        $user = $this->userService->getById($id);
        $roleOfUsers = $user->roles;
        return view('admin.users.edit', compact('user', 'roleOfUsers'));
    }

    public function update(UpdateUserRequest $request, $id)
    {
        $this->userService->update($request, $id);
        return Redirect(route('admin.users.index'))->with('success', 'Cập nhật thành công');
    }

    public function destroy($id)
    {
        $user = $this->userService->delete($id);
        return response()->json(['user' => $user, 'message' => 'Xóa người dùng thành công']);
    }
}
