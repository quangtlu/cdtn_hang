<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\Licesen\StoreLicenseRequest;
use App\Http\Requests\Admin\Licesen\UpdateLicenseRequest;
use App\Services\LicenseService;

class LicenseController extends Controller
{
    private $licenseService;

    public function __construct(LicenseService $licenseService) {
        $this->licenseService = $licenseService;
    }

    public function index()
    {
        $licenses = $this->licenseService->getPaginate();
        return view('admin.licenses.index', compact('licenses'));
    }

    public function create()
    {
        return view('admin.licenses.create');
    }

    public function store(StoreLicenseRequest $request)
    {
        $this->licenseService->create($request);
        return Redirect(route('admin.licenses.index'))->with('success', 'Thêm giấy phép thành công');
    }

    public function show($id)
    {
        $license = $this->licenseService->getById($id);
        return view('admin.licenses.show', compact('license'));
    }

    public function edit($id)
    {
        $license = $this->licenseService->getById($id);
        return view('admin.licenses.edit', compact('license'));
    }

    public function update(UpdateLicenseRequest $request, $id)
    {
        $this->licenseService->update($request, $id);
        return Redirect(route('admin.licenses.index'))->with('success', 'Cập nhật giấy phép thành công');
    }

    public function destroy($id)
    {
        $license = $this->licenseService->delete($id);
        return response()->json(['license' => $license, 'message' => 'Xóa giấy phép thành công']);
    }
}
