<?php

namespace App\Services;

use App\Models\Contract;

class ContractService
{
    private $contractModel;

    public function __construct(Contract $contractModel)
    {
        $this->contractModel = $contractModel;
    }

    public function getPaginate(){
        $contracts = Contract::latest()->paginate(10);
        return $contracts;
    }

    public function getById($id){
        $contract = $this->contractModel->findOrFail($id);   
        return $contract; 
    }

    public function create($request){
        $data = [
            "name" => $request->name,
            "description" => $request->description,
            "start_date" => $request->start_date,
            "end_date" => $request->end_date,
            "product_id" => $request->product_id,
            "user_id" => $request->user_id,
            "contract_id" => $request->contract_id,
            "license_id" => $request->license_id,
        ];

        $this->contractModel->create($data);
    }

    public function update($request, $id){
        $contract = $this->getById($id);
        $data = [
            "name" => $request->name,
            "description" => $request->description,
            "start_date" => $request->start_date,
            "end_date" => $request->end_date,
            "product_id" => $request->product_id,
            "user_id" => $request->user_id,
            "contract_id" => $request->contract_id,
            "license_id" => $request->license_id,
        ];
        $contract->update($data);
    }

    public function delete($id){
        $contract = $this->getById($id);
        $this->contractModel->destroy($id);
        return $contract;
    }

}
