<?php

namespace App\Services;
use App\Models\Product;

class ProductService

{
    private $productModel;

    public function __construct(Product $productModel)
    {
        $this->productModel = $productModel;
    }

    public function getPaginate(){
        $products = Product::latest()->paginate(10);
        return $products;
    }

    public function searchAndFilter($request)
    {
        $products = Product::query()->filterOwner($request)->filterCategory($request)->filterAuthor($request)->search($request->keyword)->paginate(10);
        return $products;
    }

    public function filter($request)
    {
        $products = Product::query()->filterOwner($request)->filterCategory($request)->filterAuthor($request)->paginate(10);
        return $products;
    }

    public function search($request)
    {
        $products = Product::search($request->keyword)->paginate(10);
        return $products;
    }

    public function getById($id){
        $product = $this->productModel->findOrFail($id);   
        return $product; 
    }

    public function create($request){
        $data = [
            "name" => $request->name,
            "pub_date" => $request->pub_date,
            "regis_date" => $request->regis_date,
            "owner_id" => $request->owner_id,
            "categoryIds" => $request->categoryIds,
        ];

        $product = $this->productModel->create($data);
        $product->authors()->attach($request->author_id);
        $product->categories()->attach($request->categoryIds);
    }

    public function update($request, $id){
        $product = $this->getById($id);
        $data = [
            "name" => $request->name,
            "pub_date" => $request->pub_date,
            "regis_date" => $request->regis_date,
            "owner_id" => $request->owner_id,
            "categoryIds" => $request->categoryIds,
        ];

        $product->update($data);
        $product->authors()->sync($request->author_id);
        $product->categories()->sync($request->categoryIds);
    }

    public function delete($id){
        $product = $this->getById($id);
        $this->productModel->destroy($id);
        $product->authors()->detach();
        $product->categories()->detach();
        return $product;
    }

}
