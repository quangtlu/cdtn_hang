<?php

namespace App\Services;

use App\Models\License;

class LicenseService

{
    private $licenseModel;

    public function __construct(License $licenseModel)
    {
        $this->licenseModel = $licenseModel;
    }

    public function getPaginate(){
        $licenses = License::latest()->paginate(10);
        return $licenses;
    }

    public function getById($id){
        $license = $this->licenseModel->findOrFail($id);   
        return $license; 
    }

    public function create($request){
        $data = [
            "name" => $request->name,
            "description" => $request->description,
            "fee" => $request->fee,
        ];

        $this->licenseModel->create($data);
    }

    public function update($request, $id){
        $license = $this->getById($id);
        $data = [
            "name" => $request->name,
            "description" => $request->description,
            "fee" => $request->fee,
        ];

        $license->update($data);
    }

    public function delete($id){
        $license = $this->getById($id);
        $this->licenseModel->destroy($id);
        return $license;
    }

}
