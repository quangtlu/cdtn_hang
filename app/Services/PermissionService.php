<?php

namespace App\Services;

use App\Models\Permission;

class PermissionService

{
    private $permissionModel;

    public function __construct(Permission $permissionModel)
    {
        $this->permissionModel = $permissionModel;
    }

    public function getAll()
    {
        $permissions = $this->permissionModel->all();
        return $permissions;
    }


    public function getPaginate()
    {
        $permissions = $this->permissionModel->latest()->paginate(10);
        return $permissions;
    }

    public function getById($id)
    {
        $permission = $this->permissionModel->findOrFail($id);   
        return $permission; 
    }

    public function create($request)
    {
        foreach ($request->action as $value) {
            $this->permissionModel->create(['name' => $value.'-'.$request->module]);
        }
    }

    public function delete($id)
    {
        return $this->permissionModel->destroy($id);
    }
}
