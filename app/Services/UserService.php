<?php

namespace App\Services;

use App\Models\User;
use Illuminate\Support\Facades\Hash;
class UserService

{

    private $userModel;

    public function __construct(User $userModel)
    {
        $this->userModel = $userModel;
    }

    public function getPaginate(){
        $users = $this->userModel->latest()->paginate(10);
        return $users;
    }

    public function getById($id){
        $user = $this->userModel->findOrFail($id);
        return $user;
    }

    public function create($request){
        

        $data = [
            "name" => $request->name,
            "gender" => $request->gender,
            "phone" => $request->phone,
            "dob" => $request->dob,
            "email" => $request->email,
            "password" => Hash::make($request->password),
        ];
        
        $user = $this->userModel->create($data);

        if ($request->roleNames) {
            $user->assignRole($request->roleNames);
        } else {
            $user->assignRole('user');
        }
    }

    public function update($request, $id){
        $user = $this->getById($id);
        
        $data = [
            "name" => $request->name,
            "phone" => $request->phone,
            "gender" => $request->gender,
            "email" => $request->email,
        ];
        if($request->password) {
            $data['password'] = Hash::make($request->password);
        }
        if($request->dob) {
            $data['dob'] = $request->dob;
        }
        $user->update($data);

        if($request->role_id) {
            $user->roles()->sync($request->role_id);
        }
    }

    public function delete($id){
        $user = $this->getById($id);
        $this->userModel->destroy($id);
        $user->roles()->detach();
        return $user;
    }

    public function getAll()
    {
        $userAll = User::all();
        return $userAll;
    }
}
