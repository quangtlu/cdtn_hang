<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Author extends Model
{
    protected $fillable = ["name", "dob", "email", "phone", "gender"];

    public function products()
    {
        return $this->belongsToMany(Product::class, 'author_product');
    }
}
