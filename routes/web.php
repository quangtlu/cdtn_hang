<?php

use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Auth;

Auth::routes();
Route::middleware('auth')->group(function () {
    //Role: admin
    Route::name('admin.')->prefix('admin')->group(function () {        
        Route::name('users.')->prefix('/users')->group(function () {
            Route::get('/', 'Admin\UserController@index')->name('index')->middleware('can:list-user');
            Route::get('/create', 'Admin\UserController@create')->name('create')->middleware('can:add-user');
            Route::post('/store', 'Admin\UserController@store')->name('store')->middleware('can:add-user');
            Route::get('/edit/{id}', 'Admin\UserController@edit')->name('edit')->middleware('can:edit-user');
            Route::post('/update/{id}', 'Admin\UserController@update')->name('update')->middleware('can:edit-user');
            Route::get('/destroy/{id}', 'Admin\UserController@destroy')->name('destroy')->middleware('can:delete-user');
        });

        Route::name('roles.')->prefix('/roles')->group(function () {
            Route::get('/', 'Admin\RoleController@index')->name('index')->middleware('can:list-role');
            Route::get('/create', 'Admin\RoleController@create')->name('create')->middleware('can:add-role');
            Route::post('/store', 'Admin\RoleController@store')->name('store')->middleware('can:add-role');
            Route::get('/edit/{id}', 'Admin\RoleController@edit')->name('edit')->middleware('can:edit-role');
            Route::post('/update/{id}', 'Admin\RoleController@update')->name('update')->middleware('can:edit-role');
            Route::get('/destroy/{id}', 'Admin\RoleController@destroy')->name('destroy')->middleware('can:delete-role');
        });

        Route::name('permissions.')->prefix('/permissions')->group(function () {
            Route::get('/', 'Admin\PermissionController@index')->name('index')->middleware('can:list-permission');
            Route::post('/store', 'Admin\PermissionController@store')->name('store')->middleware('can:add-permission');
            Route::get('/destroy/{id}', 'Admin\PermissionController@destroy')->name('destroy')->middleware('can:delete-permission');
        });

        // Role:editor or admin
        Route::name('authors.')->prefix('/authors')->group(function () {
            Route::get('/', 'Admin\AuthorController@index')->name('index')->middleware('can:list-author');
            Route::get('/create', 'Admin\AuthorController@create')->name('create')->middleware('can:add-author');
            Route::post('/store', 'Admin\AuthorController@store')->name('store')->middleware('can:add-author');
            Route::get('/edit/{id}', 'Admin\AuthorController@edit')->name('edit')->middleware('can:edit-author');
            Route::post('/update/{id}', 'Admin\AuthorController@update')->name('update')->middleware('can:edit-author');
            Route::get('/destroy/{id}', 'Admin\AuthorController@destroy')->name('destroy')->middleware('can:delete-author');
        });

        Route::name('products.')->prefix('/products')->group(function () {
            Route::get('/', 'Admin\ProductController@index')->name('index')->middleware('can:list-product');
            Route::get('/create', 'Admin\ProductController@create')->name('create')->middleware('can:add-product');
            Route::post('/store', 'Admin\ProductController@store')->name('store')->middleware('can:add-product');
            Route::get('/edit/{id}', 'Admin\ProductController@edit')->name('edit')->middleware('can:edit-product');
            Route::post('/update/{id}', 'Admin\ProductController@update')->name('update')->middleware('can:edit-product');
            Route::get('/show/{id}', 'Admin\ProductController@show')->name('show')->middleware('can:show-product');
            Route::get('/destroy/{id}', 'Admin\ProductController@destroy')->name('destroy')->middleware('can:delete-product');
        });

        Route::name('owners.')->prefix('/owners')->group(function () {
            Route::get('/', 'Admin\OwnerController@index')->name('index')->middleware('can:list-owner');
            Route::get('/create', 'Admin\OwnerController@create')->name('create')->middleware('can:add-owner');
            Route::post('/store', 'Admin\OwnerController@store')->name('store')->middleware('can:add-owner');
            Route::get('/edit/{id}', 'Admin\OwnerController@edit')->name('edit')->middleware('can:edit-owner');
            Route::post('/update/{id}', 'Admin\OwnerController@update')->name('update')->middleware('can:edit-owner');
            Route::get('/destroy/{id}', 'Admin\OwnerController@destroy')->name('destroy')->middleware('can:delete-owner');
        });

        Route::name('categories.')->prefix('/categories')->group(function () {
            Route::get('/', 'Admin\CategoryController@index')->name('index')->middleware('can:list-category');
            Route::post('/store', 'Admin\CategoryController@store')->name('store')->middleware('can:add-category');
            Route::get('/edit/{id}', 'Admin\CategoryController@edit')->name('edit')->middleware('can:edit-category');
            Route::post('/update/{id}', 'Admin\CategoryController@update')->name('update')->middleware('can:edit-category');
            Route::get('/destroy/{id}', 'Admin\CategoryController@destroy')->name('destroy')->middleware('can:delete-category');
        });

        Route::name('licenses.')->prefix('/licenses')->group(function () {
            Route::get('/', 'Admin\LicenseController@index')->name('index')->middleware('can:list-license');
            Route::get('/create', 'Admin\LicenseController@create')->name('create')->middleware('can:add-license');
            Route::post('/store', 'Admin\LicenseController@store')->name('store')->middleware('can:add-license');
            Route::get('/edit/{id}', 'Admin\LicenseController@edit')->name('edit')->middleware('can:edit-license');
            Route::post('/update/{id}', 'Admin\LicenseController@update')->name('update')->middleware('can:edit-license');
            Route::get('/destroy/{id}', 'Admin\LicenseController@destroy')->name('destroy')->middleware('can:delete-license');
        });
        
        Route::name('contracts.')->prefix('/contracts')->group(function () {
            Route::get('/', 'Admin\ContractController@index')->name('index')->middleware('can:list-contract');
            Route::get('/create', 'Admin\ContractController@create')->name('create')->middleware('can:add-contract');
            Route::post('/store', 'Admin\ContractController@store')->name('store')->middleware('can:add-contract');
            Route::get('/edit/{id}', 'Admin\ContractController@edit')->name('edit')->middleware('can:edit-contract');
            Route::post('/update/{id}', 'Admin\ContractController@update')->name('update')->middleware('can:edit-contract');
            Route::get('/destroy/{id}', 'Admin\ContractController@destroy')->name('destroy')->middleware('can:delete-contract');
            Route::get('/show/{id}', 'Admin\ContractController@show')->name('show')->middleware('can:show-contract');
        });

    });
});

Route::get('/', 'HomeController@index')->name('home.index');

